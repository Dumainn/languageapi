package fr.wailroth.language.logger;

/*
This code is owned by Alexis Dumain, aliase wailroth.
This file was created the 26/07/2021.
Ask me to use this code, at wailroth#4736.
**/


public enum Level {

    INFO, CRITICAL, FINE, ERROR, DEBUG

}


