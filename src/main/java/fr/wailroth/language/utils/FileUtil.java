package fr.wailroth.language.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileUtil {

    // -------------------------------------------- //
    // Constructor
    // -------------------------------------------- //

    public FileUtil() {
    }

    // -------------------------------------------- //
    // METHODS
    // -------------------------------------------- //


    /**
     * Create file
     *
     * @param file file
     * @throws IOException exception
     */
    public static void createFile(File file) throws IOException {
        if (!file.exists()) {
            file.getParentFile().mkdirs();
            file.createNewFile();
        }
    }


    /**
     * Save file
     *
     * @param file file
     * @param text file text
     * @throws IOException exception
     */

    public static void save( File file, String text) throws IOException {
        final FileWriter fileWriter;

        createFile(file);

        fileWriter = new FileWriter(file);
        fileWriter.write(text);
        fileWriter.flush();
        fileWriter.close();

    }


    /**
     * Load content from file
     *
     * @param file file
     * @return File content
     * @throws IOException exception
     */
    public static String loadContent(File file) throws IOException {

        if (file.exists()) {
            final BufferedReader reader = new BufferedReader(new FileReader(file));
            final StringBuilder text = new StringBuilder();

            String line;


            while ((line = reader.readLine()) != null) {
                text.append(line);
            }

            reader.close();

            return text.toString();
        }

        return "";

    }

}

