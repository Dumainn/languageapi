package fr.wailroth.language.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.util.List;

/**
 * The type Json manager.
 */
public class JsonManager {

    /**
     * The constant gson.
     */
    private static  Gson gson;
    /**
     * The Instance.
     */
    static JsonManager instance;


    /**
     * Constructor
     */
    public JsonManager() {
        gson = createGsonInstance();
    }


    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static JsonManager getInstance() {
        if(instance == null) {
            instance = new JsonManager();
        }
        return instance;
    }


    /**
     * Create gson instance
     *
     * @return Gson gson
     */
    private Gson createGsonInstance() {
        return new GsonBuilder()
                .setPrettyPrinting()
                .serializeNulls()
                .disableHtmlEscaping()
                .create();
    }


    /**
     * Serialize object
     *
     * @param object classe
     * @return String json
     */
    public String serialize(Object object) {
        return gson.toJson(object);
    }

    /**
     * Deserialize object
     *
     * @param <T>  the type parameter
     * @param json json
     * @param type class
     * @return Object class
     */
    public <T> T deserialize(String json, Class<T> type) {
        return gson.fromJson(json, type);
    }

    /**
     * Deserialize list
     *
     * @param <T>       the type parameter
     * @param json      json
     * @param typeToken the type token
     * @return list of class
     */
    public <T> List<T> deserializeList(String json, TypeToken<List<T>> typeToken) {
        return gson.fromJson(json, typeToken.getType());
    }

    /**
     * Serialize list
     *
     * @param <T>        the type parameter
     * @param objectList class
     * @return JSON string
     */
    public <T> String serializeList(List<T> objectList) {
        return gson.toJson(objectList);
    }



}


